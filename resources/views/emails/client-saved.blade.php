@component('mail::message')
# Introduction

The patient saved.

@component('mail::button', ['url' => route('clients.overview', [$client,$process_id,$step_id])])
View Patient
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
